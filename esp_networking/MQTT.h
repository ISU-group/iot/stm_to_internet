#include <PubSubClient.h>
#include "Topic.h"

PubSubClient mqtt_cli(wifiClient);

void callback(char *topic, byte *payload, unsigned int length) {

    auto topic_id = topic::topic2id(topic);

    if (topic_id != topic::TopicID::None) {
        int data = 0, order = pow(10, length - 1);  

        for (auto i = 0; i < length; i++) {
            data += int(char(payload[i]) % 48) * order;
            order /= 10;
        }

        if (data < 256 && data >= 0) {
            Serial.write(char(topic_id));
            Serial.write(char(data));            
        }
    }
}

void MQTT_init(){
    mqtt_cli.setServer(mqtt_broker, mqtt_port);
    mqtt_cli.setCallback(callback);
    while (!mqtt_cli.connected()) {
        String client_id = "esp8266-" + String(WiFi.macAddress());
        if (mqtt_cli.connect(client_id.c_str())) {} 
        else {
            delay(2000);
        }
    }  
}
