#pragma once

namespace topic {
	constexpr char* STATE_TOPIC = "isu/led_ribbon/state";
	constexpr char* BRIGHTNESS_TOPIC = "isu/led_ribbon/brightness";
  constexpr char* STATE_TOPIC_SUB = "isu/led_ribbon/state_sub";
  constexpr char* BRIGHTNESS_TOPIC_SUB = "isu/led_ribbon/brightness_sub";

   enum class TopicID : uint8_t {
      None = 0,
      State,
      Brightness,
   };

	TopicID topic2id(const char* name) {
		if (strcmp(name, STATE_TOPIC) == 0) 
			return TopicID::State;
		if (strcmp(name, BRIGHTNESS_TOPIC) == 0)
			return TopicID::Brightness;
    if (strcmp(name, BRIGHTNESS_TOPIC_SUB) == 0)
      return TopicID::Brightness;
    if (strcmp(name, STATE_TOPIC_SUB) == 0)
      return TopicID::State;

		return TopicID::None;
	}

	constexpr char* id2topic(TopicID id) {
		switch (id) {
			case TopicID::State:
				return STATE_TOPIC;
			case TopicID::Brightness:
				return BRIGHTNESS_TOPIC;
			default:
				return "";
		}
	}
}
