#include "Config.h"
#include "WIFI.h"
#include "MQTT.h"
#include "Topic.h"


void publish_if_available();

void setup(void){
    Serial.begin(115200);
    WIFI_init(false);

    // server_init();
    MQTT_init();

    pinMode(LED_BUILTIN, OUTPUT);

	mqtt_cli.subscribe(topic::STATE_TOPIC_SUB);
	mqtt_cli.subscribe(topic::BRIGHTNESS_TOPIC_SUB);
}

void loop(void){
    publish_if_available();
                 
    mqtt_cli.loop();
}

void publish_if_available() {
	while (Serial.available() > 1) {
		auto topic = topic::id2topic(static_cast<topic::TopicID>(Serial.read()));
		uint8_t value = Serial.read();

		if (strcmp(topic, "") != 0) {
			mqtt_cli.publish(topic, String(value).c_str());
        }
	}
}
