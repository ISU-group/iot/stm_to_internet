import time
import paho.mqtt.client as paho
import random
import json
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from datetime import datetime


broker="broker.emqx.io"


STATE_TOPIC = "isu/led_ribbon/state"
POTENTIOMETER_TOPIC = "isu/led_ribbon/brightness"
STATE_TOPIC_SUB = "isu/led_ribbon/state_sub"
POTENTIOMETER_TOPIC_SUB = "isu/led_ribbon/brightness_sub"


RECEIVED_MSGS = 0


def int_from_bytes(xbytes: bytes) -> int:
    return int.from_bytes(xbytes, 'big')


def on_message(client, userdata, message):
    global RECEIVED_MSGS

    if message.topic in [STATE_TOPIC, STATE_TOPIC_SUB, POTENTIOMETER_TOPIC, POTENTIOMETER_TOPIC_SUB]:
        RECEIVED_MSGS += 1

        data = str(message.payload.decode("utf-8"))
        print(f"Received message # {RECEIVED_MSGS}: {data}")
        print(f"Topic: {message.topic}")
        print(f"QoS: {message.qos}", end='\n\n')
    else:
        print(f"Message topic: {message.topic} does not match...")


client = paho.Client("client-isu-101")
client.on_message = on_message

print(f"Connecting to broker `{broker}`")
client.connect(broker)
client.loop_start()
print("Subscribing")

client.subscribe([(STATE_TOPIC, 2), (POTENTIOMETER_TOPIC, 2)], )

while True:
    pass 

client.disconnect()
client.loop_stop()
