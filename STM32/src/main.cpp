#include <mbed.h>
#include "sensor.h"

#define MAXIMUM_BUFFER_SIZE 32


static BufferedSerial serial_port(PA_9, PA_10);
static BufferedSerial esp_serial(PA_2, PA_3);

static PwmOut led(PB_5);

FileHandle* mbed::mbed_override_console(int) { return &serial_port; }


void setup();
void set_led(Sensor, uint8_t);
void listen_esp(void (*event_handler)(Sensor, uint8_t));

ssize_t send_data(Sensor, uint8_t);


int main() {
    setup();

    while (1) {
		listen_sensors([](Sensor sensor, uint8_t value) {
			send_data(sensor, value);
			set_led(sensor, value);
			ThisThread::sleep_for(100ms);
		});

        ThisThread::sleep_for(100ms);
		// /*
		// * if you want to receive
		// * just uncomment code below and comment the upper one.
		// */
		listen_esp([](Sensor sensor, uint8_t value) {
			set_led(sensor, value);
			ThisThread::sleep_for(100ms);
		});
    }
}

void setup() {
    serial_port.set_baud(9600);
	serial_port.set_format(8, BufferedSerial::None, 1);

	esp_serial.set_baud(115200);
	esp_serial.set_format(8, BufferedSerial::None, 1);

    button.mode(PullUp);
}

ssize_t send_data(Sensor sensor, uint8_t value) {
    char buf[] { char(sensor), value };
    return esp_serial.write(buf, sizeof(buf));
}

void set_led(Sensor sensor, uint8_t value) {
    switch (sensor) {
        case Sensor::Button:
            led = !led.read();
            break;
        case Sensor::Potentiometer:
            led = value / 255.0;
            break;
        default:
            break;
    }
}

void listen_esp(void (*event_handler)(Sensor, uint8_t)) {
    char buf[MAXIMUM_BUFFER_SIZE] {0};
    
    if (uint32_t read_bytes_n = esp_serial.read(buf, sizeof(buf))) {
        if (read_bytes_n > 1) {
            auto sensor = static_cast<Sensor>(buf[0]);
            event_handler(sensor, uint8_t(buf[1]));
        }
    }
}