#include "sensor.h"

#define U16_to_U8(x)    (x & 0xFFFF) >> 8
#define POTENTIOMETER_EPSILON   1


void listen_sensors(void (*event_handler)(Sensor, uint8_t)) {
    if (auto value = read_button()) {
        event_handler(Sensor::Button, value);
    }
    else {
        static uint8_t potentiometer_value = 0;
        uint8_t current_value = U16_to_U8(potentiometer.read_u16());

        /*
        *   since potentiometer can hesitate between two values
        *   which absolute difference is `1`, i.e. 58 and 59
        */
		if (abs(int(potentiometer_value) - int(current_value)) > POTENTIOMETER_EPSILON) {
			potentiometer_value = current_value;
			event_handler(Sensor::Potentiometer, potentiometer_value);
		}
    }
}

uint8_t read_button() {
    if (auto state = !button.read()) {
        ThisThread::sleep_for(20ms);
        return uint8_t(state == !button.read());
    }
    return 0;
}
