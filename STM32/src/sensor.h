#pragma once

#include <mbed.h>

static DigitalIn button(PA_1);
static AnalogIn potentiometer(PA_5);

enum class Sensor : uint8_t {
    Button = 1,
    Potentiometer,
};

void listen_sensors(void (*event_handler)(Sensor, uint8_t));

uint8_t read_button();