import time
import paho.mqtt.client as paho
import random
import json


broker = "broker.emqx.io"

STATE_TOPIC = "isu/led_ribbon/state_sub"
POTENTIOMETER_TOPIC = "isu/led_ribbon/brightness_sub"

client = paho.Client("client-isu-test-pub")

print("Connecting to broker", broker)
client.connect(broker)
client.loop_start()
print("Publishing...")

def int_to_bytes(x: int) -> bytes:
    return x.to_bytes((x.bit_length() + 7) // 8, 'big')


while True:
    
    try:
        device = int(input("Enter device: "))
    
        if device == 1:
            client.publish(STATE_TOPIC, 1)
            print(f"Just published topic: {STATE_TOPIC}")
        elif device == 2:
            value = random.randint(0, 255)
            client.publish(POTENTIOMETER_TOPIC, value)
            print(f"Just published topic: {POTENTIOMETER_TOPIC} ({value})")
    except ValueError as e:
        continue

client.disconnect()
client.loop_stop()
